# Compile stage
FROM golang:1.13.8 AS build-env

ADD . /go/src/rest-service
WORKDIR /go/src/rest-service

RUN go build -o /server -v ./cmd/apiserver

# Final stage
FROM debian:buster

EXPOSE 8000

WORKDIR /
COPY --from=build-env /server /

CMD ["/server"]