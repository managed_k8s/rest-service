package apiserver

type Config struct {
	BindAddr         string `toml:"bind_addr"`
	LogLevel         string `toml:"log_level"`
	DatabaseURL      string `toml:"database_url"`
	JenkinsHost      string `toml:"jenkins_host"`
	JenkinsUser      string `toml:"jenkins_user"`
	JenkinsToken     string `toml:"jenkins_token"`
	JenkinsCreateJob string `toml:"jenkins_create_job"`
	JenkinsDeleteJob string `toml:"jenkins_delete_job"`
}

func NewConfig() *Config {
	return &Config{
		BindAddr: ":8080",
		LogLevel: "debug",
	}
}
