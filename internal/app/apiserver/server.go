package apiserver

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"
	"net/http"
	"rest-service/internal/app/model"
	"rest-service/store"
)

type server struct {
	router *mux.Router
	logger *logrus.Logger
	store  store.Store
	config *Config
}

func newServer(store store.Store, config *Config) *server {
	s := &server{
		router: mux.NewRouter(),
		logger: logrus.New(),
		store:  store,
		config: config,
	}

	s.configureRouter()

	s.configureLogger()

	return s
}

func (s *server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}

func (s *server) configureLogger() error {
	level, err := logrus.ParseLevel(s.config.LogLevel)
	if err != nil {
		return err
	}

	s.logger.SetLevel(level)

	return nil
}

func (s *server) configureRouter() {
	s.router.HandleFunc("/clusters", s.handleClustersCreate()).Methods("POST")
}

func (s *server) handleClustersCreate() http.HandlerFunc {
	type request struct {
		Name string `json:"name"`
	}
	return func(w http.ResponseWriter, r *http.Request) {
		req := &request{}
		if err := json.NewDecoder(r.Body).Decode(req); err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		c := &model.Cluster{
			Name: req.Name,
		}

		// call jenkins job
		s.logger.Info("call jenkins job")
		client := &http.Client{}
		URL := fmt.Sprintf("http://%s/job/%s/buildWithParameters?NAME=%s",
			s.config.JenkinsHost,
			s.config.JenkinsCreateJob,
			c.Name)

		reqJenkins, err := http.NewRequest("POST", URL, nil)
		if err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		reqJenkins.SetBasicAuth(s.config.JenkinsUser, s.config.JenkinsToken)
		_, err = client.Do(reqJenkins)
		if err != nil {
			s.error(w, r, http.StatusBadRequest, err)
			return
		}

		// create record in store
		s.logger.Info("create record in store")
		if err := s.store.Cluster().Create(c); err != nil {
			s.error(w, r, http.StatusUnprocessableEntity, err)
			return
		}

		s.respond(w, r, http.StatusCreated, c)
	}
}

func (s *server) error(w http.ResponseWriter, r *http.Request, code int, err error) {
	s.respond(w, r, code, map[string]string{"error": err.Error()})
}

func (s *server) respond(w http.ResponseWriter, r *http.Request, code int, data interface{}) {
	w.WriteHeader(code)
	if data != nil {
		json.NewEncoder(w).Encode(data)
	}

}
