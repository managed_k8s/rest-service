package model

import (
	"fmt"
	validation "github.com/go-ozzo/ozzo-validation"
	"github.com/go-ozzo/ozzo-validation/is"
)

type Cluster struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	JobID int    `json:"jobid"`
}

func (c *Cluster) Validate() error {
	return validation.ValidateStruct(
		c,
		validation.Field(&c.Name, validation.Required, is.Alphanumeric),
	)
}

func (c *Cluster) BeforeCreate() error {
	if len(c.Name) == 0 {
		err := fmt.Errorf("cluster: empty name is not allowed")
		return err
	}

	return nil
}
