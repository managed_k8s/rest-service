package model_test

import (
	"github.com/stretchr/testify/assert"
	"rest-service/internal/app/model"
	"testing"
)

func TestCluster_Validate(t *testing.T) {
	testCases := []struct {
		name    string
		c       func() *model.Cluster
		isValid bool
	}{
		{
			name: "valid",
			c: func() *model.Cluster {
				return model.TestCluster(t)
			},
			isValid: true,
		},
		{
			name: "empty name",
			c: func() *model.Cluster {
				c := model.TestCluster(t)
				c.Name = ""
				return c
			},
			isValid: false,
		},
		{
			name: "invalid name",
			c: func() *model.Cluster {
				c := model.TestCluster(t)
				c.Name = "my_cluster"
				return c
			},
			isValid: false,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			if tc.isValid {
				assert.NoError(t, tc.c().Validate())
			} else {
				assert.Error(t, tc.c().Validate())
			}
		})
	}
}

func TestCluster_BeforeCreate(t *testing.T) {
	c := model.TestCluster(t)
	assert.NoError(t, c.BeforeCreate())
	assert.NotEmpty(t, c.Name)
}
