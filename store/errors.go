package store

import "errors"

var (
	ErrRecoordNotFound = errors.New("record not found")
)
