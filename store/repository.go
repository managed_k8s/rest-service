package store

import "rest-service/internal/app/model"

type ClusterRepository interface {
	Create(cluster *model.Cluster) error
	FindByName(string) (*model.Cluster, error)
}
