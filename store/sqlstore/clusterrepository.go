package sqlstore

import (
	"database/sql"
	"rest-service/internal/app/model"
	"rest-service/store"
)

type ClusterRepository struct {
	store *Store
}

func (r *ClusterRepository) Create(c *model.Cluster) error {
	if err := c.Validate(); err != nil {
		return err
	}

	if err := c.BeforeCreate(); err != nil {
		return err
	}

	return r.store.db.QueryRow(
		"INSERT INTO clusters (name) VALUES ($1) RETURNING id",
		c.Name,
	).Scan(&c.ID)
}

func (r *ClusterRepository) FindByName(name string) (*model.Cluster, error) {
	c := &model.Cluster{}
	if err := r.store.db.QueryRow(
		"SELECT id, name FROM clusters WHERE name = $1",
		name,
	).Scan(
		&c.ID,
		&c.Name,
	); err != nil {
		if err == sql.ErrNoRows {
			return nil, store.ErrRecoordNotFound
		}
		return nil, err
	}

	return c, nil
}
