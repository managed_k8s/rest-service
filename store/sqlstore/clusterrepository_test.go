package sqlstore_test

import (
	"github.com/stretchr/testify/assert"
	"os"
	"rest-service/internal/app/model"
	"rest-service/store"
	"rest-service/store/sqlstore"
	"testing"
)

func TestClusterRepository_Create(t *testing.T) {
	skipCI(t)

	db, teardown := sqlstore.TestDB(t, databaseURL)
	defer teardown("clusters")

	s := sqlstore.New(db)
	c := model.TestCluster(t)
	assert.NoError(t, s.Cluster().Create(c))
	assert.NotNil(t, c)
}

func TestClusterRepository_FindByName(t *testing.T) {
	skipCI(t)

	db, teardown := sqlstore.TestDB(t, databaseURL)
	defer teardown("clusters")

	s := sqlstore.New(db)
	name := "testcluster"
	_, err := s.Cluster().FindByName(name)
	assert.EqualError(t, err, store.ErrRecoordNotFound.Error())

	c := model.TestCluster(t)
	c.Name = name
	s.Cluster().Create(c)

	c, err = s.Cluster().FindByName(name)
	assert.NoError(t, err)
	assert.NotNil(t, c)
}

func skipCI(t *testing.T) {
	if os.Getenv("CI") != "" {
		t.Skip("Skipping testing in CI environment")
	}
}
