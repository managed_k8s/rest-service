package sqlstore

import (
	"database/sql"
	_ "github.com/lib/pq"
	"rest-service/store"
)

type Store struct {
	db                *sql.DB
	clusterRepository *ClusterRepository
}

func New(db *sql.DB) *Store {
	return &Store{
		db: db,
	}
}

func (s *Store) Cluster() store.ClusterRepository {
	if s.clusterRepository == nil {
		s.clusterRepository = &ClusterRepository{store: s}
	}

	return s.clusterRepository
}
