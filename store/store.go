package store

type Store interface {
	Cluster() ClusterRepository
}
