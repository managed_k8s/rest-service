package teststore

import (
	"rest-service/internal/app/model"
	"rest-service/store"
)

type ClusterRepository struct {
	store    *Store
	clusters map[string]*model.Cluster
}

func (r *ClusterRepository) Create(c *model.Cluster) error {
	if err := c.Validate(); err != nil {
		return err
	}

	if err := c.BeforeCreate(); err != nil {
		return err
	}

	r.clusters[c.Name] = c
	c.ID = len(r.clusters)

	return nil
}

func (r *ClusterRepository) FindByName(name string) (*model.Cluster, error) {
	c, ok := r.clusters[name]
	if !ok {
		return nil, store.ErrRecoordNotFound
	}

	return c, nil
}
