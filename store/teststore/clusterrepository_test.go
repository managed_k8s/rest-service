package teststore_test

import (
	"github.com/stretchr/testify/assert"
	"rest-service/internal/app/model"
	"rest-service/store"
	"rest-service/store/teststore"
	"testing"
)

func TestClusterRepository_Create(t *testing.T) {
	s := teststore.New()
	c := model.TestCluster(t)
	assert.NoError(t, s.Cluster().Create(c))
	assert.NotNil(t, c)
}

func TestClusterRepository_FindByName(t *testing.T) {
	s := teststore.New()
	name := "testcluster"
	_, err := s.Cluster().FindByName(name)
	assert.EqualError(t, err, store.ErrRecoordNotFound.Error())

	c := model.TestCluster(t)
	c.Name = name
	s.Cluster().Create(c)

	c, err = s.Cluster().FindByName(name)
	assert.NoError(t, err)
	assert.NotNil(t, c)
}
