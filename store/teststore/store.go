package teststore

import (
	"rest-service/internal/app/model"
	"rest-service/store"
)

type Store struct {
	clusterRepository *ClusterRepository
}

func New() *Store {
	return &Store{}
}

func (s *Store) Cluster() store.ClusterRepository {
	if s.clusterRepository == nil {
		s.clusterRepository = &ClusterRepository{
			store:    s,
			clusters: make(map[string]*model.Cluster),
		}
	}

	return s.clusterRepository
}
